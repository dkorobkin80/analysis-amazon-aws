import requests
import os

url = "https://api.twinword.com/api/text/similarity/latest/"
headers = {
            'x-rapidapi-key': "LefzglocF4ODtf2S4EVyVFZUrARVoGOK/1fAXQoQ9ZxGgZtOAL0SxgZCfwt597D8ivI7QJDT+lokn0VTn367sw==",
            'x-rapidapi-host': "api.twinword.com"
            }

# pdir = 'C:\\Programs\\Diplom\\twinword_txt'
#
# contdir = []
# for i in os.walk(pdir):
#     contdir.append(i)
#
# for i in contdir:
#     test_patent = i[2].pop()
#     for patent in i[2]:
#         file1 = test_patent.replace('.txt', '')
#         file2 = patent.replace('.txt', '')
#
#         path1 = f'C:\\Programs\\Diplom\\twinword_txt\\{file1}.txt'
#         path2 = f'C:\\Programs\\Diplom\\twinword_txt\\{file2}.txt'
#
#         text1 = open(path1, 'r').read()
#         text2 = open(path2, 'r').read()
#
#         text1, text2 = text1[:3000], text2[:3000]
#
#         querystring = {"text1": text1,
#                        "text2": text2}
#
#         response = requests.request("GET", url, headers=headers, params=querystring)
#
#         result = open(f'C:\\Programs\\Diplom\\twinword_results\\{file1}_{file2}.json', 'w')
#         result.write(response.text)
#
#         print(response.text)

path1 = 'C:\\Programs\\Diplom\\1838.txt'
path2 = 'C:\\Programs\\Diplom\\google.txt'

text1 = open(path1, 'r').read()
text2 = open(path2, 'r').read()

text1, text2 = text1[:3000], text2[:3000]

querystring = {"text1": text1,
               "text2": text2}

response = requests.request("GET", url, headers=headers, params=querystring)

result = open(f'C:\\Programs\\Diplom\\twinword_results\\1838_google.json', 'w')
result.write(response.text)
print(response.text)