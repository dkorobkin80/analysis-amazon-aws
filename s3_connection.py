import boto3
import os

client = boto3.client('s3')
path = os.getcwd()

patent_path = path + '\\patent_id_dynamodb_3'

for file in os.listdir(patent_path):
    if '.txt' in file:
        upload_file_bucket = 'patent.storage'
        upload_file_key = 'patent_id_dynamodb/' + str(file)
        client.upload_file(file, upload_file_bucket, upload_file_key)

