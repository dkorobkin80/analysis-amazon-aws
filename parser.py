from xml.dom import minidom
import os

# -------------Подключение к DynamoDB-----------
import boto3 

# Параметры подключения
table_name = 'patent'
primary_column_name = 'id'
primary_key = 48725
columns = ["date_of_publication", "patent_name", "ipc_class", "author_name(s)",
           "patent_code", "url"]

# Подключение
client = boto3.client('dynamodb')
db = boto3.resource('dynamodb')

table = db.Table(table_name)
# ---------------------------------------------

# константы
PATENT_YEAR = 2018
XML_NAME = 'ipg180724'

patent_count = 6274


def parse_all_xml(path):
    f = open(path + '\\patent_{}\\'.format(PATENT_YEAR) + '{}.xml'.format(XML_NAME), 'r')
    new_f = open('text.txt', 'w')
    patent_count = 0
    for line in f:
        if line == '<?xml version="1.0" encoding="UTF-8"?>\n':
            new_f.close()
            patent_count += 1
            new_file = open(path + '\\patent_{}\\'.format(PATENT_YEAR) + XML_NAME + '\\' + str(patent_count) + '.xml', 'w')
            new_file.write(line)
        else:
            new_file.write(line)    

    new_f.close()
    f.close()
    return patent_count


def parse_xml(path, patent_count, primary_key):
    # files = open('text.txt','w',encoding='utf-8')
    for i in range(1, patent_count+1):
        f = open(path + '\\patent_{}\\'.format(PATENT_YEAR) + XML_NAME + '\\' + str(i) + '.xml', 'r')
        try:
            xml_doc = minidom.parse(f)
        except:
            f.close()
            continue
        patent_code = get_patent_code(xml_doc)
        if patent_code == '':
            f.close()
            continue
        abstract = get_abstract(xml_doc)
        if abstract == '':
            f.close()
            continue
        description = get_description(xml_doc)
        if description == '':
            f.close()
            continue
        claims = get_claims(xml_doc)
        if claims == '':
            f.close()
            continue

        patent_name = get_patent_name(xml_doc)
        if patent_name == '':
            f.close()
            continue

        ipc_class = get_ipc_class(xml_doc)
        if ipc_class == set():
            f.close()
            continue
        date_of_pub = get_date_of_pub(xml_doc)
        inventors = get_inventors(xml_doc)
        url = 'https://s3.console.aws.amazon.com/s3/object/patent.storage?region=eu-central-1&prefix=' \
              'patent_id_dynamodb_9/{}.txt'.format(primary_key)

        # --------------------Парсинг в файлы-------------
        new_file = open(path + '\\patent_id_dynamodb_9\\' + str(primary_key) + '.txt', 'w', encoding="utf-8")
        new_file.write('patent_code: ' + patent_code + '\n')
        new_file.write('abstract: ' + abstract + '\n')
        new_file.write('description: ' + description + '\n')
        new_file.write('claim: ' + claims + '\n')
        new_file.close()
        # ------------------------------------------------

        # --------------------Парсинг в DynamoDB-------------
        response = table.put_item(
            Item={
                primary_column_name: primary_key,
                columns[0]: date_of_pub,
                columns[1]: patent_name,
                columns[2]: ipc_class,
                columns[3]: inventors,
                columns[4]: patent_code,
                columns[5]: url,
            }
        )
        # --------------------------------------------------

        primary_key += 1

        f.close()


def get_patent_code(xml_doc):
    doc_numbers = xml_doc.getElementsByTagName('doc-number')
    country = xml_doc.getElementsByTagName('country')
    kind = xml_doc.getElementsByTagName('kind')

    try:
        patent_code = country[0].firstChild.data + doc_numbers[0].firstChild.data + kind[0].firstChild.data
    except:
        patent_code = ''

    return patent_code


def get_patent_name(xml_doc):
    invention_title = xml_doc.getElementsByTagName('invention-title')[0]
    patent_name = ''
    try:
        for element in invention_title.childNodes:
            try:
                patent_name += ''.join(element.data)
            except:
                patent_name += ''.join(element.firstChild.data)
    except:
        pass

    return patent_name


def get_date_of_pub(xml_doc):
    date = xml_doc.getElementsByTagName('date')

    try:
        date_of_pub = date[0].firstChild.data 
    except:
        date_of_pub = ''

    return date_of_pub[:4] + '.' + date_of_pub[4:6] + '.' + date_of_pub[6:]


def get_ipc_class(xml_doc):
    ipc_class_lst = set()
    try:
        ipc_class = xml_doc.getElementsByTagName('classification-cpc-text')
        for i in range(len(ipc_class)):
            try:
                ipc_class_lst.add(ipc_class[i].firstChild.data)
            except:
                pass
    except:
        pass

    return ipc_class_lst


def get_inventors(xml_doc):
    inventors_lst = set()
    try:
        inventors = xml_doc.getElementsByTagName('inventors')
        inventors_first_names = inventors[0].getElementsByTagName('first-name')
        inventors_last_names = inventors[0].getElementsByTagName('last-name')

        for i in range(len(inventors_first_names)):
            try:
                inventors_lst.add(inventors_first_names[i].firstChild.data + ' ' + inventors_last_names[i].firstChild.data)
            except:
                pass
    except:
        pass

    return inventors_lst


def get_abstract(xml_doc):
    abstract = ''
    try:
        all_abstract = xml_doc.getElementsByTagName('abstract')[0]

        for all_tags in all_abstract.childNodes:
            for element in all_tags.childNodes:
                try:
                    abstract += ''.join(element.data)
                except:
                    abstract += ''.join(element.firstChild.data)
    except:
        pass

    return abstract


def get_description(xml_doc):
    number_of_descriptions = 0
    description = ''
    while True:
        try:
            descriptions = xml_doc.getElementsByTagName('description')[number_of_descriptions]
            number_of_descriptions += 1
            try:
                for all_tags in descriptions.childNodes:
                    for element in all_tags.childNodes:
                        if element.nodeType == 7:
                            pass
                        else:
                            try:
                                description += ''.join(element.data)
                                description = ' '.join(description.split())
                                description += ' '
                            except:
                                description += ''.join(element.firstChild.data)
                                description = ' '.join(description.split())
                                description += ' '


            except:
                pass

        except:
            break

    return description


def get_claims(xml_doc):
    number_of_claims = 0
    claim = ''
    while True:
        try:
            claims = xml_doc.getElementsByTagName('claim')[number_of_claims]
            number_of_claims += 1
            try:
                for all_tags in claims.childNodes:
                    for element in all_tags.childNodes:
                        if element.nodeType == 7:
                            pass
                        else:
                            try:
                                claim += ''.join(element.data)
                                claim = ' '.join(claim.split())
                                claim += ' '
                            except:
                                claim += ''.join(element.firstChild.data)
                                claim = ' '.join(claim.split())
                                claim += ' '

            except:
                pass

        except:
            break

    return claim


path = os.getcwd()
print(path)
# parse_xml(path, parse_all_xml(path))
parse_xml(path, parse_all_xml(path), primary_key)

