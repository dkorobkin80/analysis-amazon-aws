import boto3 

# Параметры подключения
table_name = 'patent'
primary_column_name = 'id'
primary_key = 1
columns = ["date_of_publication", "patent_name", "IPC_class", "author_name(s)"]

# Подключение
client = boto3.client('dynamodb')
db = boto3.resource('dynamodb')

table = db.Table(table_name)

# Использование дб
# Берем из дб
# response = table.get_item(
#     Key={
#         primary_column_name: primary_key
#     }
# )
# print(response["Item"])

# Вставляем в дб

# primary_key = 'USD0833706S1'
# response = table.put_item(
#     Item={
#         primary_column_name:primary_key,
#         columns[0] : '0',
#         columns[1] : '1',
#         columns[2] : '2',
#         columns[3] : '3',
#         columns[4] : '4'
#     }
# )
# print(response["ResponseMetadata"]["HTTPStatusCode"])

# Удаляем из дб 

# primary_key = '4'
# response = table.delete_item(
#     Key={
#         primary_column_name:primary_key
#     }
# )
# print(response)

# Описание 

# response = client.describe_table(TableName = table_name)
# print(response)

# Запрос

# from boto3.dynamodb.conditions import Key
# response = table.query(
#     KeyConditionExpression= Key(primary_column_name).eq(0)
# )
# print(response["Item"]) 

# Вся дата

from boto3.dynamodb.conditions import Attr

dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('patent')
response = table.scan(
    FilterExpression=Attr("id").gte(0)
)

classes = []
for x in response["Items"]:
    for ipc_class in x.get('ipc_class'):
        if ipc_class.split()[0] not in classes:
            classes.append(ipc_class.split()[0])

print(classes)
print(len(classes))

#10
#531
#17458



