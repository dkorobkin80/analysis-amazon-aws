import boto3
import json
# from bson import json_util

comprehend = boto3.client(service_name='comprehend', region_name='eu-central-1')

input_s3_url = "s3://patent.storage/patent_id_dynamodb_1/"
input_doc_format = "ONE_DOC_PER_FILE"
output_s3_url = "s3://topic.modeling/"
data_access_role_arn = "arn:aws:iam::771117336112:role/admin"
number_of_topics = 10
job_name = 'test'

input_data_config = {"S3Uri": input_s3_url, "InputFormat": input_doc_format}
output_data_config = {"S3Uri": output_s3_url}

start_topics_detection_job_result = comprehend.start_topics_detection_job(JobName=job_name,
                                                                          NumberOfTopics=number_of_topics,
                                                                          InputDataConfig=input_data_config,
                                                                          OutputDataConfig=output_data_config,
                                                                          DataAccessRoleArn=data_access_role_arn)

print('start_topics_detection_job_result: ' + json.dumps(start_topics_detection_job_result))

job_id = start_topics_detection_job_result["JobId"]

print('job_id: ' + job_id)

# describe_topics_detection_job_result = comprehend.describe_topics_detection_job(JobId=job_id)
#
# print('describe_topics_detection_job_result: ' + json.dumps(describe_topics_detection_job_result,
#                                                             default=json_util.default))
#
# list_topics_detection_jobs_result = comprehend.list_topics_detection_jobs()
#
# print('list_topics_detection_jobs_result: ' + json.dumps(list_topics_detection_jobs_result, default=json_util.default))