import csv
import boto3
import os

client = boto3.client('s3')
path = os.getcwd()


def csv_reader(file_obj):

    get_file_bucket = 'patent.storage'
    topic = '001'

    reader = csv.reader(file_obj)

    for row in reader:
        patent = " ".join(row).split()
        if topic == patent[1] and float(patent[2]) > 0.8:
            get_file_key = patent[0]
            file_name_index = patent[0].rfind('/')
            file_name = patent[0][file_name_index+1:]
            client.download_file(get_file_bucket, get_file_key, f'C:\\Programs\\Diplom\\twinword_txt\\{file_name}')


if __name__ == "__main__":
    csv_path = "C:\\Programs\\Diplom\\doc-topics.csv"
    with open(csv_path, "r") as f_obj:
        csv_reader(f_obj)